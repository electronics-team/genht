all:
	cd src && $(MAKE) all

install:
	cd src && $(MAKE) install

test:
	cd src && $(MAKE) test
